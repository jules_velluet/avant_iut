# -*- coding: utf-8 -*-

from PIL import Image

ext1='.jpg'
ext2='.eps'

# masque identité
noyau0 =\
[0 , 0, 0,\
0  , 1, 0,\
0  , 0, 0]

# détecteurs de contours
noyau1 =\
[-1,-1,-1,\
-1 , 8,-1,\
-1 ,-1,-1]

def filtre_contour1(image1,noyau):
    image2 = Image.new("RGB",image1.size)
    image3 = Image.new("RGB",image1.size)
    
    # conversion en niveaux de gris, mode RGB
    for y in range(image1.size[1]): # y = ligne
         for x in range(image1.size[0]): # x = colonne
             pix = image1.getpixel((x,y))
             rouge = int((pix[0]+pix[1]+pix[2])/3)
             vert = rouge
             bleu = rouge
             image2.putpixel((x,y),(rouge,vert,bleu))
    
    # filtrage de l'image en niveaux de gris
    for y in range(1,image1.size[1]-1):
        for x in range(1,image1.size[0]-1):
            liste_pix=[]
            liste_pix.append(image2.getpixel((x-1,y-1)))
            liste_pix.append(image2.getpixel((x,y-1)))
            liste_pix.append(image2.getpixel((x+1,y-1)))
            liste_pix.append(image2.getpixel((x-1,y)))
            liste_pix.append(image2.getpixel((x,y)))
            liste_pix.append(image2.getpixel((x+1,y)))
            liste_pix.append(image2.getpixel((x-1,y+1)))
            liste_pix.append(image2.getpixel((x,y+1)))
            liste_pix.append(image2.getpixel((x+1,y+1)))
            rouge=0
            for i in range(9):
                rouge=rouge+noyau[i]*liste_pix[i][0]
            
            rouge = rouge/1
            rouge = rouge + 128
            rouge = int(255-rouge)
            vert = int(rouge)
            bleu = int(rouge)
            image3.putpixel((x,y),(rouge,vert,bleu))
    
    return image3

nom_image = "billes"
mon_image = Image.open(nom_image+ext1)
filtre0 = filtre_contour1(mon_image,noyau0)
filtre1 = filtre_contour1(mon_image,noyau1)
filtre0.save(nom_image+'_contours10'+ext2)
filtre1.save(nom_image+'_contours11'+ext2)
filtre0.show()
filtre1.show()
