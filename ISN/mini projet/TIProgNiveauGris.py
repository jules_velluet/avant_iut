# -*- coding: Latin-1 -*-
# Programme de transformation d'une image couleur RGB en niveau de gris
# Dominique Lefebvre pour TangenteX.com
# 6 janvier 2016
#

# importation des librairies
import sys
from PIL import Image,ImageOps

# ouverture du fichier image
ImageFile = 'e:\PhysNumWeb1\images\hawkeye.jpg'
try:
   img = Image.open(ImageFile)
except IOError:
    print 'Erreur sur ouverture du fichier ' + ImageFile
    exit(1)

# r�cup�ration de la largeur et hauteur de l'image
colonne,ligne = img.size

# cr�ation d'une image de m�me type
imgF = Image.new(img.mode,img.size)

#boucle de traitement des pixels
for i in range(ligne):
   for j in range(colonne):
        pixel = img.getpixel((j,i)) # r�cup�ration du pixel
        # calcul du poids de chaque composante du gris dans le pixel (CIE709)
        gris = int(0.2125 * pixel[0] + 0.7154 * pixel[1] +  0.0721 * pixel[2])
        gris = int(0.33 * pixel[0] + 0.33 * pixel[1] +  0.33 * pixel[2])
        # en gris les 3 composantes RGB sont identiques
       p = (gris,gris,gris)
        # composition de la nouvelle image
       imgF.putpixel((j,i), p)


#imgF = ImageOps.grayscale(img)        

# affichage de l'image
imgF.show()

# fermeture du fichier image
img.close()

