from PIL import Image

#récupération de l'image
Imagefile="image projet.jpg"
# Ouverture de l'aimge initiale
img=Image.open(Imagefile)
img.show()

# Afin de ne pas écraser l'image initiale
# Création d'une nouvelle image avec laquelle 
# On travaille

def transformation_image(image):
        colonne, ligne=image.size
         # Création de l'image finale 
        imgF=Image.new(image.mode,image.size)#attention i en MAJUSCULE
 
        for i in range(ligne):
            for j in range(colonne):
                
                pixel = img.getpixel((j,i)) # récupération du pixel
                a=img.getpixel((j,i))
                p=(a[0]+a[1]+a[2])//3,(a[1]+a[0]+a[2])//3,(a[2]+a[0]+a[1])//3
                
                imgF.putpixel((j,i),p)
        return imgF       
  
transformation_image(img).show()
transformation_image(img).save("noir et blanc.png","PNG")
