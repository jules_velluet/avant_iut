#-*- coding: utf-8 -*-

from PIL import Image 

# Si l'image contient un nombre pair de pixels
def pixelisation(image1):
    image_gris = image1.convert('L')
    largeur,hauteur = image_gris.size
    pixel_gris = image_gris.load()
    image2 = Image.new("L",(largeur,hauteur))
    nouveau_pixel = image2.load()
    for colonne in range (..............):
        for ligne in range (..............):
            moyenne = ..........................
            nouveau_pixel[colonne,ligne] = \
            nouveau_pixel[colonne+1,ligne] = \
            nouveau_pixel[colonne,ligne+1] = \
            nouveau_pixel[colonne+1,ligne+1] = moyenne
    return image2

nom_fichier = 'billes'
ext1 = '.jpg'
ext2 = '.eps'
mon_image=nom_fichier+ext1

image_originale = Image.open(mon_image)
image_pix = pixelisation(image_originale)
image_pix.save(nom_fichier+'_pixel'+ext1)
image_pix.save(nom_fichier+'_pixel'+ext2)
image_pix.show()
