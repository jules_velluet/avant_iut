from PIL import Image

Imagefile="petite image.png"
img=Image.open(Imagefile)

def transformation_image(image):
        colonne, ligne=image.size
        imgF=Image.new(image.mode,image.size)
        for i in range(ligne):
                for j in range(colonne) :
                        a=img.getpixel((j,i))
                        p=(a[0]*2,a[1]*2,a[2]*2)
                    
                        imgF.putpixel((j,i),p)
        return imgF 
transformation_image(img).show()
transformation_image(img).save("image_éclaircit.png","PNG")
