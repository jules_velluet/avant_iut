from PIL import Image 

Imagefile="petite image.PNG"
im=Image.open(Imagefile)             
pixels=im.load()
l=im.size[0]
h=im.size[1]

def transformation_image(image):
    imout=Image.new(im.mode,im.size)
    for i in range(1,l-1):
        for j in range(1,h-1):
            a=(pixels[i-1,j-1][0]+pixels[i-1,j][0]+pixels[i-1,j+1][0]+pixels[i,j-1][0]+pixels[i,j+1][0]+pixels[i+1,j-1][0]+pixels[i+1,j][0]+pixels[i+1,j+1][0])/8
            b=(pixels[i-1,j-1][1]+pixels[i-1,j][1]+pixels[i-1,j+1][1]+pixels[i,j-1][1]+pixels[i,j+1][1]+pixels[i+1,j-1][1]+pixels[i+1,j][1]+pixels[i+1,j+1][1])/8
            c=(pixels[i-1,j-1][2]+pixels[i-1,j][2]+pixels[i-1,j+1][2]+pixels[i,j-1][2]+pixels[i,j+1][2]+pixels[i+1,j-1][2]+pixels[i+1,j][2]+pixels[i+1,j+1][2])/8
            imout.putpixel((i,j),(int(a),int(b),int(c)))
    return imout
transformation_image(im).show()
transformation_image(im).save("Image_flou.png","PNG")

