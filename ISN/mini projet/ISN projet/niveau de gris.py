from PIL import Image

Imagefile="petite image.png"
img=Image.open(Imagefile)

def transformation_image(image):
        colonne, ligne=image.size
        imgF=Image.new(image.mode,image.size)
 
        for i in range(ligne):
            for j in range(colonne):
                pixel = img.getpixel((j,i))
                a=img.getpixel((j,i))
                p=(a[0]+a[1]+a[2])//3,(a[1]+a[0]+a[2])//3,(a[2]+a[0]+a[1])//3
                
                imgF.putpixel((j,i),p)
        return imgF       
  
transformation_image(img).show()
transformation_image(img).save("noir et blanc.png","PNG")
