from PIL import Image
from HistogramToImage import *

def Blur4(image):
        ImOut = image.copy() # copie de l'image
        width,height = image.size # taille de l'image
        for indexH in range(1, height - 1): # parcours des pixels en colonne
                for indexW in range(1, width - 1): # parcours des pixels en ligne
                        value = image.getpixel((indexW, indexH))
                        value += image.getpixel((indexW, indexH - 1)) # recuperation du pixel du haut
                        value += image.getpixel((indexW, indexH + 1)) # recuperation du pixel du bas
                        value += image.getpixel((indexW - 1, indexH)) # recuperation du pixel de gauche
                        value += image.getpixel((indexW + 1, indexH)) # recuperation du pixel de droite
                        value /= 5 # moyenne
                        ImOut.putpixel((indexW, indexH), value)
        return ImOut

def Blur8(image):
        ImOut = image.copy() # copie de l'image
        
        # ...
        # A completer
        # ...
		
        return ImOut

def BlurGauss(image):
        ImOut = image.copy() # copie de l'image
        
        # ...
        # A completer
        # ...
		
        return ImOut

if __name__ == "__main__":
        
        # Initialisation
        ImIn = Image.open("#Image4.png") # ouverture de l'image
        ImGrey = ImIn.convert("L") # conversion de l'image en nuances de gris
        ImGrey.save("29_Image4_Grey.png", format="png")
        ImHistGrey = HistogramToImage(ImGrey.histogram())
        ImHistGrey.save("30_Image4_HistGrey.png", format="png")
        
        # Traitement
        ImBlur4 = Blur4(ImGrey)
        ImBlur4.save("31_Image4_Blur4.png", format="png")
        ImHistBlur4 = HistogramToImage(ImBlur4.histogram())
        ImHistBlur4.save("32_Image4_HistBlur4.png", format="png")
        
        # ...
        # A completer
        # ...
		
        # =============
        # === BONUS ===
        # =============
		
        #Initialisation
        ImIn = Image.open("#ImageCouleur2.png") # ouverture de l'image
        ImIn = ImIn.convert("RGB") # conversion de l'image en couleurs
        
        # Recuperation des trois canaux couleurs
        # ...
        # A completer
        # ...
        
        # a) Flouter les canaux couleurs
        # ...
        # A completer
        # ...
		
		# b) Flouter un seul canal a la fois
        # ...
        # A completer
        # ...