def fusion_listes_triees(liste1, liste2)->list:
    """fusionne deux listes déjà trées"""
    liste_finale= [] #contient la liste triées final
    i = 0 #indice de parcours de liste1
    j = 0 #indice de parcours de liste2
    while (i <= len(liste1)-1) and (j <= len(liste2)-1) :
        if liste1[i] <= liste2[j] :
            liste_finale.append(liste1[i])
            i += 1
        else:
            liste_finale.append(liste2[j])
            j += 1
    if i == len(liste1) :#liste1 épuisée
        liste_finale = liste_finale + liste2[j:]
    else:
        liste_finale = liste_finale + liste1[i:]



    return liste_finale
print(fusion_listes_triees([2,3,5,6],[1,2,4]))
