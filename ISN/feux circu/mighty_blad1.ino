void setup()
{
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
}

void loop()
{
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  delay(45000); // Wait for 1000 millisecond(s)
  digitalWrite(11, LOW);
  digitalWrite(12, HIGH);
  delay(5000);
  digitalWrite(12, LOW);
  digitalWrite(13, HIGH);
  digitalWrite(10, LOW);
  digitalWrite(8, HIGH);
  delay(45000);
  digitalWrite(8, LOW);
  digitalWrite(9, HIGH);
  delay(5000);
  digitalWrite(9, LOW);
  digitalWrite(13, LOW);

}

