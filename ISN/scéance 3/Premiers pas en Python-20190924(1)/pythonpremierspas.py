
# coding: utf-8

# # Python : Premières commandes

# ## Python présente la particularité de pouvoir être utilisé de plusieurs manières différentes.
# - en mode interactif via l'environnement de travail IDLE, on peut alors dialoguer directement avec lui depuis le clavier :
# ![idleimage.png](attachment:idleimage.png)
# 
# Les trois caractéres >>> constituent le signe d'invite indiquant que Python est prêt à exécuter une commande.
# - On peut aussi comme je le fais ici utiliser un logiciel qui sert d'interface, comme Spyder (ou Pyzo) ou bien Jupyter utilisé ici.!
# ![spyderimage.png](attachment:spyderimage.png)
# - Des différences : avec IDLE pour calculer 5+3 on tape 5+3 puis entrée, dans Sypder, on écrit print(5+3) puis appuie sur la touche F5 et dans la console apparaît un beau 8 !
# En fait Syper est bien adapté pour écrire des programmes longs (type projets par exemple), mais là encore c'est à l'usage que vous verrez ce qui vous convient le mieux.

# Calculer avec Python, priorité des opérations.
# 
# Tout d'abord j'importe des modules permettant d'avoir des réponses interactives.

# In[ ]:


from ipywidgets import *
from IPython.display import display


# In[ ]:


5+3


# In[ ]:


2 - 9


# Les espaces sont-ils optionnels ?

# In[ ]:


widgets.RadioButtons(
    options=['Pas répondu','oui', 'non'],
    rows=20,
    description='Réponse :',
    disabled=False
)


# In[ ]:


7 + 3 * 4


# Les priorités des opérations mathématiques sont-elles respectées ?

# In[ ]:


widgets.RadioButtons(
    options=['Pas répondu','oui', 'non'],
    rows=20,
    description='Réponse :',
    disabled=False
)


# In[ ]:


20 / 3


# In[ ]:


20 // 3


# In[ ]:


20.5 // 3


# In[ ]:


8,7 / 5


# Quelle est la différence entre / et // ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# In[ ]:


10 % 3


# In[ ]:


10 % 5


# Que fait l'opérateur % ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# In[ ]:


10**3


# In[ ]:


5**2


# Que fait l'opérateur ** ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# # Affecter une valeur à une variable, l'afficher

# In[ ]:


n=7


# In[ ]:


message = " Quoi de neuf ?"


# In[ ]:


pi=5.43


# Que fait Python ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# In[ ]:


n


# In[ ]:


message


# In[ ]:


pi 


# Que fait Python ? 

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# In[ ]:


print(message)


# In[ ]:


print(n)


# Quelle différence avec précédemment ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# In[ ]:


a=7


# In[ ]:


a


# In[ ]:


a+=1


# In[ ]:


a


# In[ ]:


a=a+1


# In[ ]:


a


# Que font les commandes a=a+1 et a+=1 ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# ## Affectations multiples

# In[ ]:


x=y=7


# In[ ]:


x


# In[ ]:


y


# In[ ]:


m, n = 7.3, 12


# In[ ]:


n


# In[ ]:


y=3*n+n/5


# In[ ]:


y


# In[ ]:


a, b = 3, 7
a = b
b = a
print(a,b)


# Commentez cette liste d'instructions :

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# In[ ]:


a, b, c, d = 3, 4, 5, 6
print(a, b, c, d)


# In[ ]:


a, b = b, a
print(a, b, c, d)


# Commentez cette liste d'instructions :

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# ## L'instruction conditionnelle : if (si en français !)

# In[ ]:


a = 0.5 
if a < 0.3 or a > 0.7 :
    print('a = ', a, 'gagné') # Notez l'indentation ici !
else :
    print('a = ',a, 'perdu') # idem


# Commentez ce petit programme :

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# ## Répétitions en boucle itérative : for

# In[ ]:


for i in range (10) :
    print('valeur de i :', i)


# In[ ]:


for i in range (1,10) :
    print('valeur de i', i)


# In[ ]:


for i in range (1,10,3):
    print('valeur de i', i)


# Commentez et bien se souvenir de cela si vous utilisez la bouble $\textbf{for}$

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# ## Répétitions à l'aide d'une boucle : l'instruction while 

# Ici on ne sait pas combien de fois on repète la même action !

# In[ ]:


a = 0
while (a < 7 ) :
    a+= 1
    print(a)


# Modifier ce programme pour faire afficher les valeurs 0, 1, 2, 3, 4, 5, 6 !

# In[ ]:


a = 0
i = 1
while (a < 7) :
    i = i+1 
    print(i,'ha')


# In[ ]:


Commentez les deux programmes 


# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# In[ ]:


a, b, c = 1, 1, 1
while (c < 11) :
    print(b, end = " ")
    a, b, c = b, a+b, c+1


# Sur une feuille de papier, écrire les différentes valeurs prises par les variables puis le résultat affiché.
# On pourra utiliser un tablaeu de ce type :

# 
# 
# || Tour 1 | Tour 2    | Tour 3 | Tour 4 | ...
# ------- || ---------------- | ---------- | ---------| -------| ------:
# a || 1 | 1|  | | | 
# b|| 1       | 2    | | | |
# c   || 1 | 2      | | | |
# affiché || 1 |2| | | | 

# Que fait ce programme ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Commentaires : ',
    disabled=False
)


# ## Mots réservés 

# Les mots suivants ne peuvent être utilisés comme nom de variables, ni comme noms de fonctions :

# and as assert break class continue def 
# del elif else except False finally for
# from global if import in is lambda
# None nonlocal not or pass raise return
# True try while with yield

# ## Type de données 

# In[ ]:


type(7)


# In[ ]:


type(3.14)


# In[ ]:


type(7.0)


# In[ ]:


type([7,4])


# In[ ]:


type('blabla')


# Quels sont les différents types rencontrés ici ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# ## La fonction print

# In[ ]:


print(7+3)


# In[ ]:


print('7+3')


# In[ ]:


print('ada', 'est','un', 'joli', 'prénom')


# In[ ]:


print('ada', 'est','un', 'joli', 'prénom',sep='*')


# In[ ]:


print('ada', 'est','un', 'joli', 'prénom',sep='')


# In[ ]:


n = 0
while (n < 6): 
    print('ada', end='X')
    n=n+1


# In[ ]:


n = 0
while (n < 6):
    print('ada',end='')
    n=n+1


# ## Quelques exercices :

# In[ ]:


h = 15
m = 27
s = 34
nbrsecondes = h*3600+m*60+s 
print('le nombre de secondes ecoulées depuis minuit est :')
print(nbrsecondes)


# Que fait ce programme ?

# In[ ]:


l = Layout(flex='0 1 auto', height='40px', min_height='40px', width='auto')
widgets.Textarea(
    layout=l,
    value='',
    placeholder='On écrit là',
    description='Réponse : ',
    disabled=False
)


# Faire la même chose avec seulement deux lignes de programmes !

# In[ ]:


# A compléter
pass 


# Ecrire un programme qui affiche : 
# ![etoiles.png](attachment:etoiles.png)
# 

# In[ ]:


# Ecrire le programme ici 
pass essai


# Fin provisoire
