from tkinter import *
from random import *
import time
import tkinter.messagebox

# Interface graphique
fenetre = Tk()
fenetre.title("Simon !")
canvas_1 = Canvas(fenetre, bg = "black", height = 500, width = 500)
canvas_1.pack(side = LEFT ,padx = 5 , pady = 5)
tours = 1
liste = [] # couleur sur les quels ont a appuyees
c = 0
boucle_de_jeu = "continue"
score = "votre score est de",tours, "!"

# Construction des couleurs.
couleurs = ['blue','red','green','yellow']

def quart_couleurs(coul):
    liste = []
    for i in range(4):
        liste.append(canvas_1.create_arc(0,0,500,500,start=0+90*i,fill=coul[i]))
    return(liste)

def setPosition(event): # Tour joueurs
    global c, resultat, score
    x = event.x
    y = event.y
    global i, resultat
    if x in range(250, 500):
        if y in range (0, 250):
            i = 0
            liste.append(0)
        else:
            i = 3
            liste.append(3)
    elif x in range (0, 250):
        if y in range (0, 250):
            i = 1
            liste.append(1)
        else:
            i = 2
            liste.append(2)
    if liste[c] == liste_couleurs_cachees[c]: # Compare les listes joueurs et nombres caches
        print(liste)
        print(liste_couleurs_cachees)
        c += 1
    else :
        tkinter.messagebox.showinfo('Vous avez perdu !', score ) # fenetre de defaites
    if len(liste) == len(liste_couleurs_cachees): # Prochain tour du BOT 
        canvas_1.update()
        c = 0
        jouer()
        quart_cache = canvas_1.create_arc(0,0,500,500,start=0+90*i,fill='black')
        canvas_1.after(200,reinitialiser)
        canvas_1.update()
    
def reinitialiser() :
    canvas_1.create_arc(0,0,500,500,start=0+90*i,fill=couleurs[i]) # On remet la couleur
    
quart_couleurs(couleurs) # met en couleur la musique


global nbr_quart_caches

# Fonction jouer

nbr_quart_caches = 0
liste_couleurs_cachees = [] # pour la suite du jeu

def jouer():
    global nbr_quart_caches, couleurs, liste_couleurs_cachees, liste, tours, nombre_defois_jouer
    nombre_defois_jouer = 0
    if boucle_de_jeu == "continue":
        if len(liste) == len(liste_couleurs_cachees):
            liste = []
            for i in range(len(liste_couleurs_cachees)):
                reinitialiser()
                canvas_1.update()
                time.sleep(1)
                quart_cache = canvas_1.create_arc(0,0,500,500,start=0+90*liste_couleurs_cachees[i],fill='black')
                canvas_1.update()
                canvas_1.create_arc(0,0,500,500,start=0+90*i,fill=couleurs[i]) # On remet la couleur
                canvas_1.update
            while nbr_quart_caches < tours :
                i = randint(0,3) # Tirage du quart à mettre en noir.
                liste_couleurs_cachees.append(i) # Pour la suite du jeu
                quart_cache = canvas_1.create_arc(0,0,500,500,start=0+90*i,fill='black')
                canvas_1.update()
                time.sleep(0.5) # 500 ms
                nbr_quart_caches += 1 
                canvas_1.create_arc(0,0,500,500,start=0+90*i,fill=couleurs[i]) # On remet la couleur
                canvas_1.update()
                canvas_1.after(500,jouer) # 500 ms
        else :
            while nombre_defois_jouer < tours :
                canvas_1.bind("<Button-1>", setPosition)
                nombre_defois_jouer += 1
            tours += 1
    
# bouton Quitter et jouer

bouton_1 = Button(fenetre, text = "Quitter", width = 8, command = fenetre.destroy)
bouton_1.pack(side = BOTTOM)
bouton_jouer = Button(fenetre, text = "Jouer", width = 8, command = jouer)
bouton_jouer.pack(side = BOTTOM)


# Observation des événements
fenetre.mainloop()
