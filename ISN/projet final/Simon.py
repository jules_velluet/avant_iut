from tkinter import *
from random import *

fenetre = Tk()
fenetre.title("Simon !")

list =[]
jeu = []
joueur = []
couleur = ['bleu', 'vert', 'jaune', 'rouge']
i = 3

def BOT():
    jeu = []
    n=1
    while n <= i:
        jeu.append(choice(couleur))
        n += 1
        a = jeu[-1]
        canvas_1.itemconfigure(bleu, fill = "black")
        fenetre.after(50,reinitialiser)
    else:
        print(a)
        
    
    
def reinitialiser() :
    canvas_1.itemconfigure(bleu, fill = "blue")
    canvas_1.itemconfigure(jaune, fill = "yellow")
    canvas_1.itemconfigure(red, fill = "red")
    canvas_1.itemconfigure(vert, fill = "green")
    
    

def setPosition(event):
    x = event.x
    y = event.y
    if x in range(250, 500):
        if y in range (0, 250):
            list.append("bleu")
            canvas_1.itemconfigure(bleu, fill = "black")
        else:
            list.append("jaune")
            canvas_1.itemconfigure(jaune, fill = "black")
    elif x in range (0, 250):
        if y in range (0, 250):
            list.append("rouge")
            canvas_1.itemconfigure(red, fill = "black")
        else:
            list.append("vert")
            canvas_1.itemconfigure(vert, fill = "black")
    fenetre.after(50,reinitialiser) 
    print(list)
        
canvas_1 = Canvas(fenetre, bg = "black", height = 500, width = 500)
canvas_1.bind("<Button-1>", setPosition)
bleu = canvas_1.create_arc(0, 0,500, 500, fill= "blue")
red = canvas_1.create_arc(0, 0,500, 500, start= 90, fill= "red")
vert = canvas_1.create_arc(0, 0,500, 500, start= 180, fill= "green")
jaune = canvas_1.create_arc(0, 0,500, 500, start= 270, fill= "yellow")
bouton_1 = Button(fenetre, text = "Quitter", width = 8, command = fenetre.destroy)
bouton_1.pack(side = BOTTOM)
bouton_jouer = Button(fenetre, text = "Jouer", width = 8, command = BOT)
bouton_jouer.pack(side = BOTTOM)
canvas_1.pack(side = LEFT ,padx = 5 , pady = 5)
fenetre.mainloop()
