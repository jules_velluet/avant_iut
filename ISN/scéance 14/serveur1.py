import socket
#initialise le serveur
connexion = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
connexion.bind(('',15555)) #port écouter
connexion.listen(2)#il peut écouter deux client
def serveur1():
    print('lancement du serveur1')
    client1 = connexion.accept()
    adresse1=client1[0]
    print('connexion établi avec :', client1)
    messageE1='bonjour client 1'.encode()
    adresse1.send(messageE1)
    messageR1 = adresse1.recv(555).decode()
    print('Message client1 :',messageR1)
    messageR2='reception OK. Au revoir'.encode()
    adresse1.send(messageR2)
    adresse1.close()
    print ('Fin serveur')

serveur1()
