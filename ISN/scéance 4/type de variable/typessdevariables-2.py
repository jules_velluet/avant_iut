
# coding: utf-8

# # Types de variables en langage Python

# # Le type nombre

# Il y a deux types de variables : les entiers, représentés par le mot clé << $\textbf{int}$>>, et les nombres réels,
# symbolisés par << $\textbf{float}$ >>.
# 
# $\texttt{Exemples :}$

# In[ ]:


a = int(input("Entrer un nombre entier :",)) # par exemple 12, le int après le = donne le type de variable


# In[ ]:


print(a) # on vérifie que tout va bien


# Cette fois on va faire une erreur de type : entrer dans b la valeur 3,7

# In[ ]:


b = int(input("Entrer un nombre entier :",)) # ici 3,7


# On peut alors remédier au problème que l'on vient de voir à l'aide de l'instruction $\textbf{eval}$ : elle identifie parfaitement 
# le type de variable !

# In[ ]:


c = eval(input("Entrer un nombre : ",)) #tester -45.2


# In[ ]:


type(c)


# In[ ]:


d = -13.4


# In[ ]:


type(d)


# In[ ]:


e = float(input('Entrer un nombre réel',)) # tester 12

print(e)

type(e)


# In[ ]:


print(e) # Ici Python renvoit 12.0 au lieu du 12 rentré


# In[ ]:


type(e)


# # Les chaînes de caractères

# Ces variables sont définies par le mot clé << $\textbf{str}$ >> et sont définies par une suite de caractères entre parenthèses ou guillements.

# In[ ]:


nom = 'Dupont'
print(nom)
print(type(nom))


# Certaines chaines peuvent poser problèmes : par exemple le mot aujourd'hui.

# In[ ]:


mot = 'aujourd'hui''
print(mot)


# Pour contourner cette difficulté : 

# In[ ]:


mot1 = 'aujourd\'hui'
print('mot1 :',mot1)
mot2 = "aujourd'hui"
print('mot2 :',mot2)


# Quelques fonctionnalités :

# In[ ]:


mot = "personnage"
print(len(mot)) # longueur de la chaîne de caractères.
print(mot[0]) # premier caractère (indice 0)
print(mot[1])  # deuxième caractère (indice 1)
print(mot[1:4]) #slicing
print(mot[2:])  # slicing
print(mot[-1]) # dernier caractère (indice -1)
print(mot[-6:]) # slicing


# In[ ]:


mot1 = "Vive l\'ISN ! "
mot2 = "on ne s'est pas trompé d\'option "
mot3 = "vite le projet pour qu\'on s\'éclate !"

mot1+mot2+mot3


# In[ ]:


mot1+mot3


# On peut donc additionner deux chaînes de caractères : on parle alors de **concaténation**.

# # Le type liste 

# Les listes sont définies par le mot clé **list** et se définissent par des données entre crochets. C'est donc une structure de données qui peut contenir des éléments de différents types.

# In[ ]:


Jours = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi']
print(Jours)


# Quelques opérations sur les listes :

# In[ ]:


# la longueur d'une liste 
print(len(Jours))


# Isoler un élément en indiquant son rang. **Attention le premier rang est 0**.

# In[ ]:


print(Jours[0])
print(Jours[2])
print(Jours[3:])


# Effacer un élément de la liste : la fonction **del**

# In[ ]:


del(Jours[2])
print(Jours)


# Ajouter un élément dans la liste : **append**
# **append** n'est pas une fonction, mais une méthode qui s'applique à l'objet Jours qui est une liste.

# In[ ]:


Jours.append('Samedi')
Jours


# # le type Booléen

# Une variable de type **booléen** est une variable qui ne peut prendre que deux valeurs : **True ou False**.
# 
# Les variables booléennes sont notamment très utiles pour réaliser une action lorsqu'une hypothèse est réalisée.

# In[ ]:


# A quoi sert cette fonction ????

def seuil(n, premier, raison) :
    a = True 
    while a : # tant que a est vrai c'est à dire à True
        premier = premier + raison
        n = n+1
        if premier >10:
            a = False 
    return n

seuil(0,2,1.25)
        


# Les opérateurs de comparaison 
# 
# |**Opérateurs**|**Signification**|**Remarque**|
# |--------------|:---------------:|:-----------:|
# | <            | Strictement inférieur à|      |
# |<=            |Inférieur ou égal à|          |
# |>             |Strictement supérieur à|      |
# |>=            |Supérieur ou égal à|          |
# |==            | Egal à|**Attention aux deux =**|
# |!=            | Différent de|                |
# 

# In[ ]:


b = 10
print(b>10)
print(b==5)
print(b!=6)


# # Le type dictionnaire

# Un dictionnaire stocke des données sous la forme clé et valeur.
# Une clé est unique et n'est pas nécessairement un entier (comme c'est le cas de l'indice d'une liste).

# In[ ]:


moyennes = {'math': 12.5, 'anglais': 15.8}# entre accolades
print(type(moyennes))

print(moyennes['anglais']) # entre crochets

moyennes['anglais'] = 14.3# nouvelle affectation
print(moyennes)

moyennes['sport'] = 11.0# nouvelle entrée
print(moyennes)


# # Contenu d'un module : le module math

# In[ ]:


import math


# In[ ]:


dir(math) # affiche les fonctions et les données du module math


# Pour appeler une fonction d'un module, la syntaxe est la suivante :
# **module.fonction(arguments)**
# 
# Pour accéder à une donnée d'un module :
# **module.data**

# In[ ]:


print(math.pi)# donnée pi du module math (nombre pi)

print(math.sin(math.pi/4.0)) # fonction sin() du module math (sinus)

print(math.sqrt(2.0)) # fonction sqrt() du module math (racine carrée)

print(math.sqrt(5)) # racine de 5...


print(math.exp(-3.0))# fonction exp() du module math (exponentielle)

print(math.log(math.e))# fonction log() du module math (logarithme népérien)


