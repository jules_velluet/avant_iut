def somme_inv_carres(n:int)->float:
    """Calcul de la somme des inverse des carrés d'entiers"""
    somme = 0 #la somme calculée est initialisée à 0#
    for i in range(1,n+1):
        somme = somme + 1/(i**2) # cumul somme précédente et 1/(i**2)#
    return somme

assert(somme_inv_carres(2) == 1,25)
print(somme_inv_carres(4))

for i in range(1,100000):
    print(somme_inv_carres(i))
