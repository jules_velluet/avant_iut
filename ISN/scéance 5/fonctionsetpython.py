
# coding: utf-8

# # Fonctions et Python

# -Pour l'instant, nous avons eu affaire à des programmes plutôt courts, ne nécessitant pas plus d'une dizaine de lignes.
# 
# -Bien évidemment ce n'est pas le cas de la majorité des programmes, et nous allons être amenés à décomposer certains programmes en pluisuers sous-programmes. Les fonctions peuvent répondre à une partie des ces exigeances.
# 
# -Elles trouvent également une vocation mathématique, à savoir être appelées à plusieurs endroits différents d'un  même programme.
# 
# -Et comme en mathématiques, pas de fonctions sans variables !

# ## La syntaxe

# Elle est la suivante :
# 
# 
# def nomdelafonction(liste des paramètres):
#      .... bloc d'instructions ... ne pas oublier l'indentation
#     

# ## Fonction sans paramètre

# On va définir une fonction qui calcule les premières valeurs de la table de 11, ce qui peut toujours être utile !

# In[4]:


def table11(): # programme sans paramètres, on met quand même les parenthèses.
    for i in range (10):
        a = 11*i
        print(a, end=' ')


# In[5]:


table11() # imprime la table demandée


# ## Fonctions avec paramètres 

# Pour avoir accès à toutes les tables de multiplications, il est alors intéressant d'avoir la valeur de la table voulue comme paramètre.
# 
# Nous allons don créer une autre fonction qui calcule les 10 premiers termes d'une table de multiplication dont nous entrerons ultérieurement la valeur. Pour cela on définit la fonction suivante : 

# In[6]:


def table(base): # base est la variable 
    for i in range(10):
        a = i*base
        print(a, end=' ')


# Il suffit alors d'appeler cette fonction dans un programme en transmettant à la fonction un argument qui va se substituer au paramètre :

# In[7]:


table(3)


# In[8]:


table(45)


# In[10]:


table('ha')


# Et oui, là on peut faire déjà pas mal de choses ! C'est fou non ? Un dernier :

# In[11]:


L=[1,2,3,4] # L est une liste
table(L)


# On a obtenu une concaténation de la liste L par elle même !

# On peut aussi faire en sorte que l'argument transmis soit aussi une variable :
# 

# In[12]:


def table(base):
    print('Voici la table de ', base, ':')
    for i in range (10):
        a = i*base
        print(a, end=' ')
        
for k in range (3,10):
    table(k)
    print(' ')


# Exercice : écrire un programme, utilisant une fonction, qui calcule les valeurs de $i^2$ pour $i$ allant de 0 à une valeur entière $n$ donnée comme paramètre.

# In[14]:


# Calcul des n^2 à compléter
def carres(...) :
    pass


# ## Variables locales, variables globales

# La variable $\textbf{base}$ est une variable $\textbf{locale}$. Elle n'existe que dans la fonction table. Si l'on voulait affixher la valeur de base à l'extérieur de la fonction, on aurait un message d'erreur, la preuve :

# In[1]:


print(base)


# L'avantage est de pouvoir créer des variables locales sans se soucier de leurs noms puisque, même si une variable globale porte le même nom, sa valeur ne sera pas modifiée !

# In[3]:


def table(base):
    print('Voici la table de ', base, ':')
    for i in range (10):
        a = i*base
        print(a, end=' ')

base = 3         
for k in range (3,10):
    table(k)
    print(' ')
    
print('La base reste à la valeur', base, 'et pour la fonction.')


# Lorsque l'on veut définir une variable globale dans une fonction on utilise l'instruction global.

# In[4]:


def table() :
    global base
    print('Voici la table de ',base, ':')
    for i in range(10):
        a=i*base
        print(a, end=' ')
    base = base + 1
    
base = 2
table()


# Comme on a dans la fonction l'instruction base = base + 1, on peut alors tester table() :

# In[5]:


table()


# A nouveau : 

# In[6]:


table()


# ## Fonctions prédéfinies

# >Il en existe une infinité, présentes souvent dans des bibliothèques que l'on peut appeler en début de programme :
# 
# > from math import *
# 
# >from turtle import *
# 
# >from random import *

# In[7]:


from random import randint # import la fonction randint du module random

for i in range(1,10) :
    print(randint(1,3))


# ## L'instruction return

# Pour l'instant, les fonctions que nous avons rencontrées, ne renvoient pas de résultats. 
# 
# Elles se contentent d'afficher un résultat grâce à l'instruction $\textbf{print}$. Pour que la fonction contienne un résultat, il faut utiliser l'instruction $\textbf{return}$.

# In[19]:


def t(base) :
    for i in range(10):
        print(base*i, end= ' ')

t(8)


# A noter : si l'on affecte à une variable notée ici a, le contenu de la fonction t, avec la variable 5, on obtient la table de 5 dans la liste mais la variable a ne contient...

# In[20]:


a=t(5)


# In[21]:


print(a)


# In[22]:


type(a)


# L'utilisation de return permet d'éviter le problème !

# In[27]:


def t(base):
    l=[] # on utilise une liste car la fonction ne renvoit qu'une valeur
    for i in range(10):
        a = base * i 
        l.append(a)
    return l  # la fonction renvoit la liste l sans l'afficher


# In[28]:


t(5)


# In[29]:


a=t(5)


# In[32]:


print(a)


# In[31]:


type(a)

