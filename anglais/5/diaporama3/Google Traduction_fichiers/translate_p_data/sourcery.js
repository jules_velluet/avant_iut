var referralTracker = {

    //config
    startup_ny_tracked_params: ['utm_source', 'utm_medium', 'utm_campaign', 'ext_id'],
    get_started_form_fields: ['ACQUISITION_SOURCE', 'ACQUISITION_MEDIUM', 'ACQUISITION_CAMPAIGN', 'EXTERNAL_MEDIA_ID', 'INTERNAL_WEB_ID'],

    getReferralInfo: function() {
        for (x = 0; x < referralTracker.startup_ny_tracked_params.length; x++) {
            param = referralTracker.startup_ny_tracked_params[x];
            val = referralTracker.getParameterByName(param);
            if (val != '') {
                document.cookie = param + "=" + val + "; path=/";
            }
        }
    },

    getParameterByName: function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    generateGUID: function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },

    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    },

    setCookie: function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    },

    trackUser: function() {
        if (referralTracker.getCookie('user_id') == "") {
            user_id = referralTracker.generateGUID();
            referralTracker.setCookie('user_id', user_id, 1826);
        }
    },

    addFormData: function(formData) {
        form_params = referralTracker.startup_ny_tracked_params.slice(0)
        form_params.push('user_id');
        for (var x = 0; x < form_params.length; x++) {
            form_name = referralTracker.get_started_form_fields[x];
            form_val = referralTracker.getCookie(form_params[x]);
            formData[form_name] =  form_val;
        }
    }
}
referralTracker.getReferralInfo();
referralTracker.trackUser();